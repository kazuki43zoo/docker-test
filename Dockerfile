FROM mysql:5.7

COPY src/main/resources/mysql/schema.sql /docker-entrypoint-initdb.d/
