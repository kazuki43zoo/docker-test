DROP TABLE IF EXISTS t_users;

CREATE TABLE t_users
(
    id    varchar(256) primary key,
    name  varchar(256) not null,
    email varchar(256) not null,
    unique uk_t_users (email)
);
